﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// The following code generates a circle.
    /// </summary>
    public class Circle: IShape
    {
        /// <summary>
        /// enter values of circle.
        /// </summary>
        public int x, y, radius;
        /// <summary>
        /// Circle base
        /// </summary>
        public Circle() : base()
        {

        }

        /// <param name="x">x-axis</param>
        /// <param name="y">y-axis</param>
        /// <param name="radius">radius of a circle</param>
        public Circle(int x, int y, int radius)
        {

            this.radius = radius; 
        }
        /// <param name="g"> graphics parameter</param>
        public void draw(Graphics g)
        {
            try
            {
                Pen p = new Pen(Color.Black, 2);
                SolidBrush b = new SolidBrush(Color.Red);
                g.DrawEllipse(p, x - radius, y - radius, radius * 2, radius * 2);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
        /// <param name="list"></param>
        public void set(params int[] list)
        {
            try
            {
                this.x = list[0];
                this.y = list[1];
                this.radius = list[2];

            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
    }
}
