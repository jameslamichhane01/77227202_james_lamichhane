﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// Generates Triangle.
    /// </summary>
    public class Triangle: IShape
    {
        /// <summary>
        /// Enter triangle height.
        /// </summary>
        public int x, y, width, height;
        /// <summary>
        /// Triangle.
        /// </summary>
        public Triangle() : base()
        {
            width = 0;
            height = 0;
        }
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Triangle(int x, int y, int width, int height)
        {

            this.width = width;
            this.height = height;
        }
        /// <param name="g"></param>
        public void draw(Graphics g)
        {
            try
            {
                Point[] p = new Point[3];
                p[0].X = x;
                p[0].Y = y - (height / 2);

                p[1].X = x - (width / 2);
                p[1].Y = y + (height / 2);

                p[2].X = x + (width / 2);
                p[2].Y = y + (height / 2);
                Pen po = new Pen(Color.Red);
                g.DrawPolygon(po, p);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }


        /// <summary>
        /// Set params.
        /// </summary>
        /// <param name="list"></param>
        public void set(params int[] list)
        {
            try
            {
                this.x = list[0];
                this.y = list[1];
                this.width = list[2];
                this.height = list[3];
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
