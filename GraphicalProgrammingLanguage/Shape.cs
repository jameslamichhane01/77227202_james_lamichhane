﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicalProgrammingLanguage
{
    /// <summary>
    /// Graphical programming language.
    /// </summary>
    public interface IShape
    {
        /// <summary>
        /// Draw graphics.
        /// </summary>
        /// <param name="g"></param>
        void draw(Graphics g);
        /// <summary>
        /// enter params.
        /// </summary>
        /// <param name="list"></param>
        void set(params int[] list);
    }
}
